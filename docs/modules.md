[scarf5](README.md) / Exports

# scarf5

## Table of contents

### Properties

- [default](modules.md#default)

## Properties

### default

• **default**: *object*

#### Type declaration:

| Name | Type |
| :------ | :------ |
| `Game` | *typeof* Game |
