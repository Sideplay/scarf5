import * as PIXI from "pixi.js";

export class Sprite extends PIXI.Sprite {
  public constructor() {
    super(PIXI.Texture.EMPTY);
  }

  public makeBlue() {
    this.tint = 0x0000ff;
  }
}
