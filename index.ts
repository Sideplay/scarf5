import { Game } from "./src/core/Game";
import { Sprite } from "./src/objects/Sprite";

export default { Game, Sprite };
